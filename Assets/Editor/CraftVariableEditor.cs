﻿//editor code that create a better experience for crafting variable handling

using CraftSystem.Core.ConstValues;
using Data.Craft;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    [CustomEditor(typeof(CraftVariables))]
    public class CraftVariableEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var craftVariables = (CraftVariables) target;

            EditorGUILayout.LabelField("Chance To Craft");
            EditorGUILayout.LabelField("");
            EditorGUILayout.LabelField("Percent : " + craftVariables.ChanceToCraft);

            craftVariables.ChanceToCraft = EditorGUILayout.Slider(craftVariables.ChanceToCraft, 0, 100);
            craftVariables.ChanceToCraft = Mathf.RoundToInt(craftVariables.ChanceToCraft);
            
            EditorGUILayout.LabelField("");

            EditorGUILayout.LabelField("Critical Damage Chance");
            EditorGUILayout.LabelField("");
            EditorGUILayout.LabelField("Min : " + craftVariables.CriticalDamageChanceMin
                                                + " --- "
                                                + "Max : " + craftVariables.CriticalDamageChanceMax);

            EditorGUILayout.MinMaxSlider(ref craftVariables.CriticalDamageChanceMin,
                ref craftVariables.CriticalDamageChanceMax,
                ChanceConst.CRITICAL_DAMAGE_CHANCE_MIN, ChanceConst.CRITICAL_DAMAGE_CHANCE_MAX);
            
            craftVariables.CriticalDamageChanceMin = Mathf.RoundToInt(craftVariables.CriticalDamageChanceMin);
            craftVariables.CriticalDamageChanceMax = Mathf.RoundToInt(craftVariables.CriticalDamageChanceMax);
            
            EditorGUILayout.LabelField("");

            EditorGUILayout.LabelField("Strength Range");
            EditorGUILayout.LabelField("");
            EditorGUILayout.LabelField("Min : " + craftVariables.StrengthRangeMin
                                                + " --- "
                                                + "Max : " + craftVariables.StrengthRangeMax);

            EditorGUILayout.MinMaxSlider(ref craftVariables.StrengthRangeMin,
                ref craftVariables.StrengthRangeMax,
                ChanceConst.STRENGTH_RANGE_MIN, ChanceConst.STRENGTH_RANGE_MAX);
            
            craftVariables.StrengthRangeMin = Mathf.RoundToInt(craftVariables.StrengthRangeMin);
            craftVariables.StrengthRangeMax = Mathf.RoundToInt(craftVariables.StrengthRangeMax);
            
            EditorGUILayout.LabelField("");

            EditorGUILayout.LabelField("Fire Damage Range");
            EditorGUILayout.LabelField("");
            EditorGUILayout.LabelField("Min : " + craftVariables.FireDamageRangeMin
                                                + " --- "
                                                + "Max : " + craftVariables.FireDamageRangeMax);

            EditorGUILayout.MinMaxSlider(ref craftVariables.FireDamageRangeMin,
                ref craftVariables.FireDamageRangeMax,
                ChanceConst.FIRE_DAMAGE_RANGE_MIN, ChanceConst.FIRE_DAMAGE_RANGE_MAX);
            
            craftVariables.FireDamageRangeMin = Mathf.RoundToInt(craftVariables.FireDamageRangeMin);
            craftVariables.FireDamageRangeMax = Mathf.RoundToInt(craftVariables.FireDamageRangeMax);
            
            EditorGUILayout.LabelField("");
        }
    }
}