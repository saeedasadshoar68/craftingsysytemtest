﻿//holds collectible item data that user can collect

using GameEnums;
namespace Data.Collectible
{
    [UnityEngine.CreateAssetMenu(fileName = "CollectibleItem", menuName = "Items/Collectible", order = 0)]
    public class CollectibleItem : UnityEngine.ScriptableObject
    {
        public int SpaceRequired = 1;
        public ItemIds ItemId;
        public CollectibleItemType CollectibleItemType;
        public string Description;
    }
}