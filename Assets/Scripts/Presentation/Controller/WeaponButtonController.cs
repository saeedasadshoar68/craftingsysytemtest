﻿using CraftSystem.Core.Enum;
using CraftSystem.Core.Interface;
using Data.Weapon;
using InventorySystem.Core.Interface;
using IOC.Signals;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Presentation.Controller
{
    public class WeaponButtonController : UiButtonController
    {
        [SerializeField] private Text label;
        [SerializeField] private WeaponInstruction _weaponInstruction;
        private IInventoryService _inventoryService;
        private ICraftSystemService _craftSystemService;

        [Inject]
        private void Init(IInventoryService inventoryService,
            SignalBus signalBus,
            ICraftSystemService craftSystemService)
        {
            _inventoryService = inventoryService;
            _craftSystemService = craftSystemService;

            signalBus.Subscribe<OnItemAddedToInventory>(OnItemAddedToInventory);
            signalBus.Subscribe<OnItemUsedFromInventory>(OnItemAddedToInventory);
            Invoke(nameof(Load), 1);
        }

        private void OnItemAddedToInventory()
        {
            CheckRequirements();
        }

        private void Load()
        {
            CheckRequirements();
        }

        private bool CheckRequirements()
        {
            string required = "";
            var canCreate = true;
            foreach (var requirement in _weaponInstruction.CraftRequirements)
            {
                if (!_inventoryService.CheckRequireItem(requirement.Item.ItemId.ToString(), requirement.Count))
                {
                    required += requirement.Item.ItemId + " - ";
                    canCreate = false;
                }
            }

            Debug.Log(!canCreate ? required : "Can Craft Now");

            GetComponent<Button>().image.color = canCreate ? Color.green : Color.red;
            return canCreate;
        }

        public override void ButtonClicked()
        {
            base.ButtonClicked();

            if (!CheckRequirements()) return;

            var result = _craftSystemService.CreateWeaponFromInstruction(_weaponInstruction);
            label.text = result.CraftMessage + "\n";

            if (result.CraftResultState != CraftResultState.Succeed) return;

            label.text += "Strength: " + result.WeaponData.Strength + "\n";
            label.text += "CriticalDamage: " + result.WeaponData.CriticalDamage + "\n";
            label.text += "FireDamage: " + result.WeaponData.FireDamage + "\n";
        }
    }
}