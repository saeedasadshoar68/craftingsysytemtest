﻿//each requirement for craft something like weapon stores in this class

using System;
using Data.Collectible;

namespace Data.Craft
{
    [Serializable]
    public struct CraftRequirement
    {
        public CollectibleItem Item;
        public int Count;
    }
}