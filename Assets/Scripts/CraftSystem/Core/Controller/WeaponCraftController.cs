﻿using CraftSystem.Core.Data;
using CraftSystem.Core.Enum;
using CraftSystem.Core.Interface;
using Data.Craft;
using Data.Weapon;
using UnityEngine;

namespace CraftSystem.Core.Controller
{
    public class WeaponCraftController : IWeaponCraftController
    {
        public WeaponCraftResult TryToCraft(WeaponInstruction craftInstruction)
        {
            var craftVariables = craftInstruction.CraftVariables;
            var weaponData = craftInstruction.Weapon;

            float randomVal = Random.Range(0, 100);
            var result = new WeaponCraftResult();
            
            if (randomVal > craftVariables.ChanceToCraft)
            {
                result.CraftMessage = "No Chance random: " + randomVal + " - ChanceToCraft: " +
                                      craftVariables.ChanceToCraft;
                result.WeaponData = weaponData;
                result.CraftResultState = CraftResultState.FailedByChance;
            }
            else
            {
                weaponData.FireDamage = CalculateFireDamage(craftVariables);
                weaponData.Strength = CalculateStrength(craftVariables);
                weaponData.CriticalDamage = CalculateCriticalDamage(craftVariables);
                
                result.CraftMessage = "Succeed";
                result.WeaponData = weaponData;
                result.CraftResultState = CraftResultState.Succeed;
            }

            return result;
        }

        private float CalculateFireDamage(CraftVariables craftVariables)
        {
            return Random.Range(craftVariables.FireDamageRangeMin, craftVariables.FireDamageRangeMax);
        }

        private float CalculateStrength(CraftVariables craftVariables)
        {
            return Random.Range(craftVariables.StrengthRangeMin, craftVariables.StrengthRangeMax);
        }

        private float CalculateCriticalDamage(CraftVariables craftVariables)
        {
            return Random.Range(craftVariables.CriticalDamageChanceMin, craftVariables.CriticalDamageChanceMax);
        }
    }
}