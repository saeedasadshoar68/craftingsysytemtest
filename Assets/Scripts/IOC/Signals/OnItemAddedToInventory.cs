﻿//Class for fire and Listen to add to inventory -- when any item added to to inventory we use this class
using InventorySystem.Core.Data;

namespace IOC.Signals
{
    public class OnItemAddedToInventory
    {
        public InventoryItem InventoryItem;
        
        public OnItemAddedToInventory(InventoryItem inventoryItem)
        {
            InventoryItem = inventoryItem;
        }
    }
}