//this is the IOC class of zenject plugin
//Zenject handles the dependency injection in our test project

using CraftSystem.Core.Controller;
using CraftSystem.Core.Interface;
using CraftSystem.Core.Service;
using InventorySystem.Core.Data;
using InventorySystem.Core.Interface;
using InventorySystem.Core.Service;
using InventorySystem.Presentation.Controller;
using IOC.Signals;
using ResourceSystem.Interface;
using UnityEngine;
using Zenject;

namespace IOC.Core
{
    [CreateAssetMenu(fileName = "IocInstaller", menuName = "Installers/IocInstaller")]
    public class IocInstaller : ScriptableObjectInstaller<IocInstaller>
    {
        [SerializeField] private GameObject inventoryUiItemPrefab;

        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);
        
            Container.DeclareSignal<OnItemAddedToInventory>().OptionalSubscriber();
            Container.DeclareSignal<OnItemUsedFromInventory>().OptionalSubscriber();
            
            Container.Bind<IResourceSystem>().FromComponentInHierarchy().AsSingle().NonLazy();
            
            Container.Bind<IInventoryService>().To<InventoryService>().AsSingle().NonLazy();
            Container.Bind<IInventoryData>().To<InventoryDataHolder>().AsSingle().NonLazy();
            
            Container.Bind<ICraftSystemService>().To<CraftSystemService>().AsSingle().NonLazy();
            Container.Bind<IWeaponCraftController>().To<WeaponCraftController>().AsSingle().NonLazy();
            
            Container.BindFactory<EachInventoryItemController, EachInventoryItemController.Factory>()
                .FromComponentInNewPrefab(inventoryUiItemPrefab);
            
        }
    }
}