﻿using Data.Collectible;
using InventorySystem.Core.Data;

namespace InventorySystem.Core.Interface
{
    public interface IInventoryData
    {
        InventoryItem Add(CollectibleItem item,int count);
        InventoryItem Use(CollectibleItem item,int count);
        bool CheckExists(string id);
        bool CanUse(string id,int count);
        int CalculateEmptySpace();
    }
}