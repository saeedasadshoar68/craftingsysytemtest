﻿//each inventory item is a collectible item with count of it that we have
using Data.Collectible;

namespace InventorySystem.Core.Data
{
    public class InventoryItem
    {
        public InventoryItem(CollectibleItem item, int itemCount)
        {
            CollectibleItem = item;
            count = itemCount;
        }

        public CollectibleItem CollectibleItem;
        public int count;
    }
}