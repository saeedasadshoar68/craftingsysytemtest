﻿using InventorySystem.Core.Interface;
using UnityEngine.UI;
using Zenject;

namespace Presentation.Controller
{
    public class AddToInventoryButton : UiButtonController
    {
        public Dropdown itemDropDown;
        private IInventoryService _inventoryService;

        [Inject]
        private void Init(IInventoryService inventoryService)
        {
            _inventoryService = inventoryService;
        }

        public override void ButtonClicked()
        {
            base.ButtonClicked();
            _inventoryService.AddToInventory(itemDropDown.captionText.text, 1);
        }
    }
}