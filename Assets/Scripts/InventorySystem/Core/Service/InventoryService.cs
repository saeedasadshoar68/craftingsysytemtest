﻿//this service handle the usage of inventory 

using InventorySystem.Core.Interface;
using IOC.Signals;
using ResourceSystem.Interface;
using Zenject;

namespace InventorySystem.Core.Service
{
    public class InventoryService : IInventoryService
    {
        private IInventoryData _inventoryData;
        private IResourceSystem _resourceSystem;
        private SignalBus _signalBus;

        [Inject]
        public void Init(IInventoryData inventoryData,
            IResourceSystem resourceSystem,
            SignalBus signalBus)
        {
            _inventoryData = inventoryData;
            _resourceSystem = resourceSystem;
            _signalBus = signalBus;
        }

        public void AddToInventory(string id,int count)
        {
            var item = _resourceSystem.SelectCollectibleById(id);
            if (item != null)
            {
                var inventoryItem = _inventoryData.Add(item,count);
                _signalBus.Fire(new OnItemAddedToInventory(inventoryItem));
            }
        }

        public void UseFromInventory(string id, int count)
        {
            var item = _resourceSystem.SelectCollectibleById(id);
            if (item != null)
            {
                var inventoryItem =_inventoryData.Use(_resourceSystem.SelectCollectibleById(id), count);
                _signalBus.Fire(new OnItemUsedFromInventory(inventoryItem));
            }
        }

        public bool CheckRequireItem(string id,int count)
        {
            return _inventoryData.CanUse(id, count);
        }
        
    }
}