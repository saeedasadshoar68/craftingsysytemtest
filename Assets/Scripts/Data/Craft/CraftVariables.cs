﻿//crafting chances
using UnityEngine;

namespace Data.Craft
{
    [CreateAssetMenu(fileName = "CraftVariables", menuName = "Items/CraftVariables", order = 0)]
    public class CraftVariables : ScriptableObject
    {
        public float ChanceToCraft;
        public float CriticalDamageChanceMin;
        public float CriticalDamageChanceMax;
        public float FireDamageRangeMin;
        public float FireDamageRangeMax;
        public float StrengthRangeMin;
        public float StrengthRangeMax;
    }
}