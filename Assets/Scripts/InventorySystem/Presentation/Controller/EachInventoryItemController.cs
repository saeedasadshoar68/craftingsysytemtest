﻿//Handle the ui object that shows the specific inventory item

using InventorySystem.Core.Data;
using InventorySystem.Presentation.View;
using UnityEngine;
using Zenject;

namespace InventorySystem.Presentation.Controller
{
    public class EachInventoryItemController : MonoBehaviour
    {
        private InventoryItem _inventoryItem;
        private EachInventoryItemView _eachInventoryItemView;

        [Inject]
        private void Init()
        {
            _eachInventoryItemView = GetComponent<EachInventoryItemView>();
        }

        public void Load(InventoryItem inventoryItem)
        {
            _inventoryItem = inventoryItem;
            _eachInventoryItemView.SetName(_inventoryItem.CollectibleItem.ItemId.ToString());
            LoadValue();
        }

        public void LoadValue()
        {
            _eachInventoryItemView.SetCount(_inventoryItem.count);
        }

        public string GetItemId()
        {
            return _inventoryItem.CollectibleItem.ItemId.ToString();
        }

        public class Factory : PlaceholderFactory<EachInventoryItemController>
        {
        }
    }
}