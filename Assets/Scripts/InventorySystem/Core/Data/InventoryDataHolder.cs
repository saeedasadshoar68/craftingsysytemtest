﻿//this class holds items in the inventory for future use

using System.Collections.Generic;
using Data.Collectible;
using InventorySystem.Core.Interface;

namespace InventorySystem.Core.Data
{
    public class InventoryDataHolder : IInventoryData
    {
        private readonly int _maxCapacity = 100;
        private Dictionary<string, InventoryItem> _inventoryItems;

        public InventoryItem Add(CollectibleItem item, int count)
        {
            _inventoryItems ??= new Dictionary<string, InventoryItem>();
            var key = item.ItemId.ToString();
            InventoryItem inventoryItem = null;
            
            if (!CheckExists(key))
            {
                inventoryItem = new InventoryItem(item, count);
                _inventoryItems.Add(key, inventoryItem);
            }
            else
            {
                inventoryItem = _inventoryItems[key];
                inventoryItem.count += count;
            }

            return inventoryItem;
        }

        public InventoryItem Use(CollectibleItem item, int count)
        {
            _inventoryItems ??= new Dictionary<string, InventoryItem>();

            if (!CanUse(item.ItemId.ToString(), count)) return null;

            var inventoryItem = _inventoryItems[item.ItemId.ToString()];
            inventoryItem.count -= count;
            if (inventoryItem.count <= 0)
            {
                _inventoryItems.Remove(item.ItemId.ToString());
            }

            return inventoryItem;
        }

        public bool CheckExists(string id)
        {
            _inventoryItems ??= new Dictionary<string, InventoryItem>();
            if (_inventoryItems.ContainsKey(id))
            {
                return true;
            }

            //Debug.Log($"Item: {id} does not exist");
            return false;
        }

        public bool CanUse(string id, int count)
        {
            if (!CheckExists(id)) return false;

            var inventoryItem = _inventoryItems[id];
            if (inventoryItem.count >= count)
            {
                return true;
            }

            //Debug.Log($"There isn't enough from item :{id}");
            return false;
        }

        public int CalculateEmptySpace()
        {
            _inventoryItems ??= new Dictionary<string, InventoryItem>();
            var occupied = 0;
            foreach (var item in _inventoryItems.Values)
            {
                occupied += item.count;
            }

            return _maxCapacity - occupied;
        }
    }
}