﻿namespace GameEnums
{
    public enum CollectibleItemType
    {
        WeaponCraftInstructions,
        CraftingItem
    }
}