﻿//by using this service we try to craft weapon when it has all the items needed according to instruction

using CraftSystem.Core.Data;
using CraftSystem.Core.Enum;
using CraftSystem.Core.Interface;
using Data.Craft;
using Data.Weapon;
using InventorySystem.Core.Interface;
using UnityEngine;
using Zenject;

namespace CraftSystem.Core.Service
{
    public class CraftSystemService : ICraftSystemService
    {
        private IInventoryService _inventoryService;
        private IWeaponCraftController _weaponCraftController;

        [Inject]
        private void Init(IInventoryService inventoryService,
            IWeaponCraftController weaponCraftController)
        {
            _inventoryService = inventoryService;
            _weaponCraftController = weaponCraftController;
        }

        public WeaponCraftResult CreateWeaponFromInstruction(WeaponInstruction instruction)
        {
            foreach (var requirement in instruction.CraftRequirements)
            {
                _inventoryService.UseFromInventory(requirement.Item.ItemId.ToString(), requirement.Count);
            }

            var weapon = _weaponCraftController.TryToCraft(instruction);
            return weapon;
        }
    }
}