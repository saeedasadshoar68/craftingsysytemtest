﻿//by having this data in inventory player can create weapon according to this instruction
//each weapon can have list of requirements and specific crafting variables
using System.Collections.Generic;
using Data.Craft;
using UnityEngine;

namespace Data.Weapon
{
    [CreateAssetMenu(fileName = "WeaponInstruction", menuName = "Items/WeaponInstruction", order = 0)]
    public class WeaponInstruction : ScriptableObject
    {
        public WeaponData Weapon;
        public CraftVariables CraftVariables;
        public List<CraftRequirement> CraftRequirements;
    }
}