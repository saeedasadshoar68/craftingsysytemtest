﻿namespace Presentation.Interface
{
    public interface IUiButton
    {
        public void ButtonClicked();
    }
}