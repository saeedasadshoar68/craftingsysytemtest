﻿//we use this const variables to define min and max of our chance, this is just for test,
//for best result we need to load from server api

namespace CraftSystem.Core.ConstValues
{
    public static class ChanceConst
    {
        public const float CRITICAL_DAMAGE_CHANCE_MIN = 0;
        public const float CRITICAL_DAMAGE_CHANCE_MAX = 50;
        public const float FIRE_DAMAGE_RANGE_MIN = 0;
        public const float FIRE_DAMAGE_RANGE_MAX = 50;
        public const float STRENGTH_RANGE_MIN = 0;
        public const float STRENGTH_RANGE_MAX = 50;
    }
}