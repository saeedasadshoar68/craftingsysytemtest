﻿//This class created for handle sound effect and other general features on buttons click...


using Presentation.Interface;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Presentation.Controller
{
    [RequireComponent(typeof(Button))]
    public abstract class UiButtonController : MonoBehaviour, IUiButton
    {
        private Button _buttonComponent;

        [Inject]
        private void Init()
        {
            _buttonComponent = GetComponent<Button>();
            _buttonComponent.onClick.AddListener(ButtonClicked);
        }

        public virtual void ButtonClicked()
        {
        }
    }
}