﻿using CraftSystem.Core.Data;
using Data.Weapon;

namespace CraftSystem.Core.Interface
{
    public interface ICraftSystemService
    {
        WeaponCraftResult CreateWeaponFromInstruction(WeaponInstruction instruction);
    }
}