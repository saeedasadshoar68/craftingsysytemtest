﻿namespace CraftSystem.Core.Enum
{
    public enum CraftResultState
    {
        None,
        FailedByChance,
        Succeed
    }
}