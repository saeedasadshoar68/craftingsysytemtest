﻿//each weapon data

using UnityEngine;

namespace Data.Weapon
{
    [CreateAssetMenu(fileName = "WeaponData", menuName = "Items/WeaponData", order = 0)]
    public class WeaponData : ScriptableObject
    {
        public float Damage;
        public float CriticalDamage;
        public float FireDamage;
        public float Strength;
    }
}