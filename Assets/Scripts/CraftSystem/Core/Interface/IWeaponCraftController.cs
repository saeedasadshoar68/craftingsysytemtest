﻿using CraftSystem.Core.Data;
using Data.Weapon;

namespace CraftSystem.Core.Interface
{
    public interface IWeaponCraftController
    {
        WeaponCraftResult TryToCraft(WeaponInstruction craftInstruction);
    }
}