﻿//use this class for store the result of crafting.
//if crafting was failed because of bad luck, in the CraftResultState we will see
//FailedByChance result otherwise ith will be sucsees and with craft message we will get the
//progress of crafting result

using CraftSystem.Core.Enum;
using Data.Weapon;

namespace CraftSystem.Core.Data
{
    public class WeaponCraftResult
    {
        public WeaponData WeaponData;
        public string CraftMessage;
        public CraftResultState CraftResultState;
        
        public WeaponCraftResult()
        {
            WeaponData = null;
            CraftMessage = "";
            CraftResultState = CraftResultState.None;
        }
    }
}