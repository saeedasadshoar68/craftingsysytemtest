﻿using System.Collections.Generic;
using InventorySystem.Presentation.Controller;
using IOC.Signals;
using UnityEngine;
using Zenject;

namespace Presentation.Controller
{
    public class InventoryScrollController: MonoBehaviour
    {
        public Transform InventoryItemParent;
        
        private List<EachInventoryItemController> _inventoryViewItems;
        private EachInventoryItemController.Factory _eachItemFactory;
        private SignalBus _signalBus;

        [Inject]
        private void Init(SignalBus signalBus,
            EachInventoryItemController.Factory eachItemFactory)
        {
            _signalBus = signalBus;
            _signalBus.Subscribe<OnItemAddedToInventory>(ItemAddedToInventory);
            _signalBus.Subscribe<OnItemUsedFromInventory>(ItemUsedFromInventory);
            _eachItemFactory = eachItemFactory;
        }

        private void ItemUsedFromInventory(OnItemUsedFromInventory usedItem)
        {
            _inventoryViewItems ??= new List<EachInventoryItemController>();
            foreach (var inventoryButtons in _inventoryViewItems)
            {
                if (inventoryButtons.GetItemId().Equals(usedItem.InventoryItem.CollectibleItem.ItemId.ToString()))
                {
                    inventoryButtons.LoadValue();
                    return;
                }
            }
        }

        private void ItemAddedToInventory(OnItemAddedToInventory addedItem)
        {
            _inventoryViewItems ??= new List<EachInventoryItemController>();
            foreach (var inventoryButtons in _inventoryViewItems)
            {
                if (inventoryButtons.GetItemId().Equals(addedItem.InventoryItem.CollectibleItem.ItemId.ToString()))
                {
                    inventoryButtons.LoadValue();
                    return;
                }
            }

            var createdItem = _eachItemFactory.Create();
            createdItem.transform.SetParent(InventoryItemParent);
            createdItem.transform.localScale=Vector3.one;
            createdItem.Load(addedItem.InventoryItem);
            
            _inventoryViewItems.Add(createdItem);
        }
    }
}