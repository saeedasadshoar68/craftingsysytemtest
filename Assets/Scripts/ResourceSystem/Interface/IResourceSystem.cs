﻿using Data.Collectible;

namespace ResourceSystem.Interface
{
    public interface IResourceSystem
    {
        CollectibleItem SelectCollectibleById(string id);
    }
}