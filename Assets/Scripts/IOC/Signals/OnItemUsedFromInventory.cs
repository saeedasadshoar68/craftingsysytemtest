﻿//Class for fire and Listen to use from inventory -- when any item use from to inventory we use this class
using InventorySystem.Core.Data;

namespace IOC.Signals
{
    public class OnItemUsedFromInventory
    {
        public InventoryItem InventoryItem;
        
        public OnItemUsedFromInventory(InventoryItem inventoryItem)
        {
            InventoryItem = inventoryItem;
        }
    }
}