﻿//View of each Inventory item viewer

using UnityEngine;
using UnityEngine.UI;

namespace InventorySystem.Presentation.View
{
    public class EachInventoryItemView : MonoBehaviour
    {
        [SerializeField]private Text _itemName;
        [SerializeField]private Text _itemCount;

        public void SetName(string itemName)
        {
            _itemName.text = itemName;
        }
        
        public void SetCount(int count)
        {
            _itemCount.text = count.ToString();
        }
    }
}