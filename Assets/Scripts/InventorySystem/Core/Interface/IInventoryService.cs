﻿using Data.Collectible;

namespace InventorySystem.Core.Interface
{
    public interface IInventoryService
    {
        void AddToInventory(string id,int count);
        
        void UseFromInventory(string id,int count);
        
        bool CheckRequireItem(string id, int count);
    }
}