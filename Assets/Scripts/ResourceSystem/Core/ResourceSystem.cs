﻿//holds resource prefab and scriptable objects and a way for select them

using System.Collections.Generic;
using Data.Collectible;
using ResourceSystem.Interface;
using UnityEngine;

namespace ResourceSystem.Core
{
    public class ResourceSystem : MonoBehaviour, IResourceSystem
    {
        [SerializeField] private List<CollectibleItem> _collectibles;

        public CollectibleItem SelectCollectibleById(string id)
        {
            foreach (var item in _collectibles)
            {
                if (item.ItemId.ToString().Equals(id))
                {
                    return item;
                }
            }

            return null;
        }
    }
}